package be.kdg.curriculumvitae.activities

import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.ConfigurationCompat
import androidx.core.os.bundleOf
import be.kdg.curriculumvitae.R
import be.kdg.curriculumvitae.adapters.StudyAdapter
import be.kdg.curriculumvitae.fragments.StudyDetailFragment

/**
 * Deze activity implementeert 'OnStudySelectedListener'!
 * Als er een study aangeklikt wordt dan zal deze klasse in actie moeten schieten.
 */
class MainActivity : AppCompatActivity(), StudyAdapter.OnStudySelectedListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    /**
     * Hier reageren we op het feit dat een study aangeklikt wordt.
     */
    override fun onStudySelected(studyIndex: Int) {
        val orientation = resources.configuration.orientation
        if (orientation== Configuration.ORIENTATION_PORTRAIT){
            startActivity(Intent(this, StudyDetailActivity::class.java).apply {
                putExtra(STUDY_INDEX, studyIndex)
            })
        } else {
            val fragment = supportFragmentManager.findFragmentById(R.id.fragmentRechts) as StudyDetailFragment
            fragment.setStudyIndex(studyIndex)
        }

    }
}
