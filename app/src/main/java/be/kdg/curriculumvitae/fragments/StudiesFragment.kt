package be.kdg.curriculumvitae.fragments

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import be.kdg.curriculumvitae.R
import be.kdg.curriculumvitae.adapters.StudyAdapter
import be.kdg.curriculumvitae.databinding.FragmentStudiesBinding
import be.kdg.curriculumvitae.model.getStudies
import java.lang.Exception

class StudiesFragment : Fragment() {
    private lateinit var listener: StudyAdapter.OnStudySelectedListener
    private lateinit var binding: FragmentStudiesBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentStudiesBinding.inflate(inflater, container, false)
        binding.root.findViewById<RecyclerView>(R.id.studies)
            .apply {
                adapter = StudyAdapter(getStudies(), context, listener)
                layoutManager = LinearLayoutManager(context)
            }

        return binding.root
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is StudyAdapter.OnStudySelectedListener)
            listener = context
        else throw Exception("Context is of wrong type")
    }
}