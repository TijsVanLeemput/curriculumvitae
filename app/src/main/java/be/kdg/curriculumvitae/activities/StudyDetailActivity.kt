package be.kdg.curriculumvitae.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import be.kdg.curriculumvitae.R
import be.kdg.curriculumvitae.fragments.StudyDetailFragment

const val STUDY_INDEX = "STUDY_INDEX"

class StudyDetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_study_detail)

        // TODO: aanvullen
        val studyIndex= intent.getIntExtra(STUDY_INDEX,0)

        val fragment = supportFragmentManager.findFragmentById(R.id.fragment2) as StudyDetailFragment
        fragment.setStudyIndex(studyIndex)
    }
}
