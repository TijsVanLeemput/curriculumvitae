package be.kdg.curriculumvitae.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import be.kdg.curriculumvitae.R
import be.kdg.curriculumvitae.databinding.FragmentStudyDetailBinding
import be.kdg.curriculumvitae.model.getStudies

class StudyDetailFragment : Fragment() {
    var index: Int = 0
    private lateinit var binding: FragmentStudyDetailBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentStudyDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    fun updateFields() {
        val image = binding.root.findViewById<ImageView>(R.id.image)
        val name = binding.root.findViewById<TextView>(R.id.name)
        val info = binding.root.findViewById<TextView>(R.id.info)
        val beginDate = binding.root.findViewById<TextView>(R.id.beginDate)
        val endDate = binding.root.findViewById<TextView>(R.id.endDate)

        val currentStudy = getStudies()[index]

        image.setImageResource(currentStudy.imageResource)
        name.setText(currentStudy.school)
        info.setText(currentStudy.comment)
        beginDate.setText(currentStudy.startYear.toString())
        endDate.setText(currentStudy.endYear.toString())
    }

    fun setStudyIndex(studyIndex: Int){
        index=studyIndex;
        updateFields()
    }
}