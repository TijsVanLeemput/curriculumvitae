package be.kdg.curriculumvitae.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.content.res.AppCompatResources
import androidx.recyclerview.widget.RecyclerView
import be.kdg.curriculumvitae.databinding.StudyListItemBinding
import be.kdg.curriculumvitae.model.Study

class StudyAdapter(
  private val studies: Array<Study>,
  private val context: Context,
  private val listener: OnStudySelectedListener
) : RecyclerView.Adapter<StudyViewHolder>() {


  override fun onCreateViewHolder(vg: ViewGroup, layoutId: Int): StudyViewHolder {
    //  inflaten met de ViewBinding klasse en doorgeven aan de ViewHolder
    val binding = StudyListItemBinding.inflate(LayoutInflater.from(vg.context), vg, false)
    return StudyViewHolder(binding)
  }

  override fun getItemCount(): Int = studies.size

  override fun onBindViewHolder(vh: StudyViewHolder, index: Int) {
    val study = studies[index]
    vh.schoolImage.setImageDrawable(AppCompatResources.getDrawable(context, study.imageResource))
    vh.yearsField.text = "${study.startYear.toString()}  - ${study.endYear.toString()}"
    vh.schoolField.text = study.school

    // Klikt er iemand op een study? -> dan spreken we 'listener' aan
    vh.itemView.setOnClickListener {
      listener.onStudySelected(index)
    }
  }

  // Deze interface moet geimplementeerd worden indien
  // je wil reageren op het selecteren van een study.
  interface OnStudySelectedListener {
    fun onStudySelected(studyIndex: Int)
  }
}

// De ViewHolder gebruikt de binding
// De superklasse heeft wel de view nodig die in de root van de binding zit
class StudyViewHolder(binding: StudyListItemBinding) : RecyclerView.ViewHolder(binding.root) {
  val schoolImage: ImageView = binding.schoolImage
  val schoolField: TextView = binding.schoolField
  val yearsField: TextView = binding.yearsField
}
